#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

// SDL Deps.
#include <SDL.h>
#include <SDL_ttf.h>

#define WIDTH 640
#define HEIGHT 360

void print_text(SDL_Renderer* render, char* text, float x, float y, TTF_Font* font) {

  SDL_Color White = {0, 0, 0};
  SDL_Rect box;

  box.x = x;
  box.y = y;

  SDL_Surface* surface =
      TTF_RenderText_Solid(font, text, White); 
  box.w = surface->w;
  box.h = surface->h;

  SDL_Texture* texture = SDL_CreateTextureFromSurface(render, surface);

  SDL_RenderCopy(render, texture, NULL, &box);

  SDL_FreeSurface(surface);
  SDL_DestroyTexture(texture);
  texture = NULL;
  surface = NULL;
}

int main(int argc, char* argv[]) {
  bool inited = true;
  if(SDL_Init(SDL_INIT_VIDEO) != 0) { printf("%s %s\n", "SDL INIT ERROR!", SDL_GetError()); inited = false; }
  if(TTF_Init() != 0) { printf("%s %s\n", "SDL TTF ERROR!", TTF_GetError()); inited = false; }

  TTF_Font* font = TTF_OpenFont("./res/JetBrainsMonoNLNerdFontMono-Bold.ttf", 24);

  if(font == NULL) inited = false;

  if(!inited) {
    printf("%s\n", "Init failed");
    return 1;
  }
  
  // Setup window
  SDL_Window* window;
  SDL_Renderer* render;

  SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, SDL_WINDOW_SHOWN, &window, &render);

  int x_input = 0;
  int y_input = 0;

  float x_pos = 0;
  float y_pos = 0;
  float x_vel = 0;
  float y_vel = 0;

  // Game loop
  bool running = true;

  uint64_t ticks_last_frame = SDL_GetPerformanceCounter();
  while(running) {
    uint64_t ticks_now = SDL_GetPerformanceCounter();
    float deltaTime = (float)(ticks_now - ticks_last_frame) / SDL_GetPerformanceFrequency();
    ticks_last_frame = ticks_now;

    SDL_Event event;
    const Uint8* KEY = SDL_GetKeyboardState(NULL);
    while(SDL_PollEvent(&event)) {
      switch(event.type) {
        case SDL_QUIT:
          running = false;
          break;
      }
    }
    x_input = KEY[SDL_SCANCODE_D] - KEY[SDL_SCANCODE_A];
    y_input = KEY[SDL_SCANCODE_S] - KEY[SDL_SCANCODE_W];
    x_vel = x_input * 60;
    y_vel = y_input * 60;
    x_pos += x_vel * deltaTime;
    y_pos += y_vel * deltaTime;

    SDL_SetRenderDrawColor(render, 255, 255, 255, 255);
    SDL_RenderClear(render);

    print_text(render, "text", x_pos, y_pos, font);

    SDL_SetRenderDrawColor(render, 0, 0, 0, 255);
    SDL_Rect rect;
    for(int i = 0; i < WIDTH; i++) {
      rect.x = i;
      rect.y = sin((float) (i / 10.0)) * 50 + HEIGHT / 2;
      rect.w = 5;
      rect.h = 5;
      SDL_RenderFillRect(render,&rect);
    }
    SDL_RenderPresent(render);
  }

  // Clean up
  SDL_DestroyWindow(window);
  SDL_DestroyRenderer(render);
  return 0;
}
